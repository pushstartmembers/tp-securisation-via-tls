# DOJO TLS POULE START (Push Ton Start #8)

## Préparation
### Pré-requis
* Avoir Docker d'installé sur sa machine
* Avoir **Bash** (natif sous Linux, disponible sous **Windows 10**, ou avec **Git Bash** si version antérieure)
* Avoir accès au fichier **/etc/hosts** (ou **C:\windows\system32\drivers\etc\hosts** sous Windows)

### Mise en place de l'environnement
* Clonez ce repo et positionnez-vous à l'intérieur
* Lancement du serveur \
`docker-compose up -d`

* Récupérer l'IP du serveur \
`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' server`

* Récupérer l'IP du client \
`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' client`

* Ajouter les entrées suivantes dans le fichier **hosts** \
`IP_SERVEUR server www.poulestart.org` \
`IP_CLIENT client`

### Serveur
`ssh user@server -p 2222` (mot de passe: user) \
`cd TP_TLS`

### Client
`ssh user@client -p 2222` (mot de passe: user) \
`cd TP_TLS`

## Création d'une Autorité de Certification Racine (Auto Signée)
### Serveur
Génération clé privée: \
`openssl genrsa -out CA.priv 2048`

Extraction clé publique:  \
`openssl rsa -in CA.priv -pubout -out CA.pub`

Création de l'Autorité de Certification: \
`openssl req -x509 -new -nodes -key CA.priv -days 365 -out CA.pem`

Copie de la clé publique et du certificat de l'Autorité de Certification sur le client: \
`scp -P 2222 CA.pub user@client:/config/TP_TLS/` \
`scp -P 2222 CA.pem user@client:/config/TP_TLS/`

## Comparaison clé privée et clé publique
### Client
Génération clé privée: \
`openssl genrsa -out cle.privee 2048`

Extraction de la clé publique: \
`openssl rsa -in cle.privee -pubout -out cle.publique`

Extraction au format texte: \
`openssl rsa -in cle.privee -noout -text > cle.privee.txt` \
`openssl rsa -pubin -in cle.publique -noout -text > cle.publique.txt`

Ouvrir les extractions sur le PC et les comparer. Qu'en déduisez-vous ?

## Chiffrement et signature d'un fichier
### Client
Création d’un fichier texte: \
`echo "Ceci est un fichier ultra secret" > fichier.txt`

Chiffrement avec la clé publique de l’Autorité de Certification: \
`openssl rsautl -encrypt -pubin -inkey CA.pub -in fichier.txt -out fichier.txt.chiffre`

Copie du fichier chiffré sur le serveur: \
`scp -P 2222 fichier.txt.chiffre user@server:/config/TP_TLS/`

### Serveur
Déchiffrement du fichier avec la clé privée de l’Autorité de Certification: \
`openssl rsautl -decrypt -inkey CA.priv -in fichier.txt.chiffre > fichier.txt`

Afficher le contenu du fichier: \
`cat fichier.txt`

Que voyez-vous ?

Signature du fichier:\
`openssl rsautl -sign -inkey CA.priv -in fichier.txt -out fichier.txt.signe`

Copier fichier.signe sur le client: \
`scp -P 2222 fichier.txt.signe user@client:/config/TP_TLS/`

### Client
Vérification de la signature du fichier avec la clé publique du client:  \
`openssl rsautl -verify -pubin -inkey cle.publique -in fichier.txt.signe` 

Pourquoi y a-t-il une erreur ?

Vérification de la signature du fichier avec la clé publique du serveur: \
`openssl rsautl -verify -pubin -inkey CA.pub -in fichier.txt.signe`

C'est mieux comme ça ?

## Création d'un certificat Serveur signé par l'Autorité de Certification
### Serveur
Génération clé privée: \
`openssl genrsa -out cle.privee 2048`

Création de la requête de certification, préciser en CN (Common Name) l'URL du serveur (ex: www.poulestart.org): \
`openssl req -new -key cle.privee -out requeteCertificationServeur.csr`

Signature du certificat par l'Autorité de Certification: \
`openssl x509 -days 10 -CA CA.pem -CAkey CA.priv -CAcreateserial -req -in requeteCertificationServeur.csr -out certificatServeur.pem`

Extraction au format texte le certificat et la requête: \
`openssl x509 -in certificatServeur.pem -noout -text > certificatServeur.txt` \
`openssl req -in requeteCertificationServeur.csr -noout -text > requeteCertificationServeur.csr.txt`

Ouvrir les extractions sur le PC et les comparer. Que remarquez-vous ?

Créer une chaîne de certificats: \
`cat certificatServeur.pem CA.pem > chaineDeCertificats.pem`

Activation TLS One Way -> Modifier la configuration SSL: \
`sudo cp /etc/apache2/conf.d/ssl.conf /etc/apache2/conf.d/ssl.conf.bkp` \
`sudo cp CONF/ssl.conf.TlsOneWay /etc/apache2/conf.d/ssl.conf` \
`sudo httpd -k restart`

### Sur votre Firefox:
Se rendre sur https://www.poulestart.org, prêter attention à l'avertissement de sécurité car l'Autorité de Certification n'est pas reconnue.

Pour l'ajouter :

Outils > Préférences > Afficher les certificats \
Dans la section "Autorités", cliquer sur "Importer" puis choisir le fichier "CA.pem" \
Se rendre sur https://www.poulestart.org, l'avertissement a disparu et le cadenas est présent dans la barre d'adresse.

## Création d'un certificat Client signé par l'Autorité de Certification
### Client
Création de la requête de certification: \
`openssl req -new -key cle.privee -out requeteCertification.csr`

Copie de la requête de certification sur le serveur: \
`scp -P 2222 requeteCertification.csr user@server:/config/TP_TLS/`

### Serveur
Création du certificat signé: \
`openssl x509 -days 10 -CA CA.pem -CAkey CA.priv -CAcreateserial -req -in requeteCertification.csr -out certificat.pem`

Copie du certificat sur le client: \
`scp -P 2222 certificat.pem user@client:/config/TP_TLS/`

### Client
Création de l’enveloppe P12: \
`openssl pkcs12 -export -in certificat.pem -inkey cle.privee -name "Client" -caname "Push Start" -out client.p12` 

Pour les curieux qui veulent regarder comment ça se passe à l’intérieur: \
`openssl pkcs12 -info -in client.p12`

Télécharger les Fichiers client.p12 et CA.pem sur le PC

### Serveur
Activation TLS Two Ways -> Modifier la configuration SSL: \
`sudo cp CONF/ssl.conf.TlsTwoWays /etc/apache2/conf.d/ssl.conf` \
`sudo httpd -k restart`

### Sur votre Firefox:
Se rendre sur https://www.poulestart.org, pourquoi avous-nous une erreur ?

Outils > Préférences > Afficher les certificats \
Dans la section "Vos certificats", cliquer sur "Importer" puis choisir le fichier "client.p12" (entrer le mot de passe) \
Se rendre sur https://www.poulestart.org et choisir le certificat Client.

Voilà c'est fait !!!

## Démontage de l'environnement
* Arrêt du client et du serveur \
`docker-compose down`

* Suppression des fichiers créés \
`rm -f CLIENT/*.*` \
`rm -f SERVEUR/*.*`

* Suppression des hosts dans le fichier **hosts**

* Suppression des autorités & certificats dans Firefox

* Suppression des empruntes SSH importées \
`ssh-keygen -f "/home/hvtf4514/.ssh/known_hosts" -R "[client]:2222"` \
`ssh-keygen -f "/home/hvtf4514/.ssh/known_hosts" -R "[server]:2222"`